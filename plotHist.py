#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

############################################################
################       Load Data       #####################
############################################################

resultsFile = 'fullInclusionCycle-nonlocalFIP.txt'
nonlocalFip = np.loadtxt(resultsFile,delimiter=',',dtype=float)

lastFip = nonlocalFip[:,-1]

plt.hist(lastFip,bins=np.linspace(1.e-8,25e-7,20))


# plt.rcParams.update({'font.size': 14})
# plt.figure(2)
# plt.plot(strainX,stressX,'k')
# #plt.plot(strainData,stressData,'r')
# plt.xlabel('Strain',fontsize=18)
# plt.ylabel('Stress, MPa',fontsize=18)
# plt.gcf().subplots_adjust(left=0.20)
# plt.gcf().subplots_adjust(bottom=0.20)


plt.show()
