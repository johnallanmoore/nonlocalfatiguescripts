import math
import datetime
from odbAccess import *
from abaqusConstants import *

import numpy as np

# extract FIP only from a specific elset region
inElset = True

# elset to extract FIP in
elsetName = 'MATRIX'

# Input and ouput file names
InputName='fullInclusionCycle'

# fatigi socie scaling parameter
kappa = 0.55

# yield strength
sy = 750.

# plane normal
n = np.matrix.transpose(np.array([cos(pi/4.), -cos(pi/4.), 0.0]))
t = np.array([cos(pi/4.),  cos(pi/4.), 0.0])

# let you know what ODB is open
print 'ODB = ' + InputName

#print 'Output Variable = ' + OutName4
outputfilename1=InputName+'-FIP'+'.txt'

## open txt file to write to
out1 = open(outputfilename1,'w')

# opend .dob file to read from
odb = openOdb(InputName+'.odb')

# number of steps
steps = odb.steps.keys()
numSteps = len(steps)

# get element set
assembly = odb.rootAssembly.instances['PART-1-1']
elemset = assembly.elementSets[elsetName]

# number of elements
frame = odb.steps[steps[0]].frames[-1]
stressVar=frame.fieldOutputs['S']
if inElset == True:
        stressVar = stressVar.getSubset(region=elemset)
numelem = len(stressVar.values)

storeStressN = np.zeros((numelem,numSteps))
storeGammaP = np.zeros((numelem,numSteps))
storeElem = np.zeros((numelem,),dtype=int)



for sNum, s in enumerate(steps):

        # number of frames in step
        numberFrame = len(odb.steps[s].frames)

        # extract last frame 
        frame = odb.steps[s].frames[-1]

        # number of elements
        stressVar  = frame.fieldOutputs['S']
        strainpVar = frame.fieldOutputs['PE']

        if inElset == True:
                stressVar = stressVar.getSubset(region=elemset)
                strainpVar = strainpVar.getSubset(region=elemset)

        numelem = len(stressVar.values)

        # these just let you know its running and how long it takes
        print str(datetime.datetime.now())

        for e in range (0,numelem):
                if sNum == 0:
                        storeElem[e] = stressVar.values[e].elementLabel
                stress = np.zeros((3,3))
                #11,22,33,12,13,23,
                stress[0,0] = stressVar.values[e].data[0]
                stress[1,1] = stressVar.values[e].data[1]
                stress[2,2] = stressVar.values[e].data[2]
                stress[0,1] = stressVar.values[e].data[3]
                stress[0,2] = stressVar.values[e].data[4]
                stress[1,2] = stressVar.values[e].data[5]

                stress[1,0] = stress[0,1]
                stress[2,0] = stress[0,2]
                stress[2,1] = stress[1,2]

                strainp = np.zeros((3,3))
                #11,22,33,12,13,23,
                strainp[0,0] = strainpVar.values[e].data[0]
                strainp[1,1] = strainpVar.values[e].data[1]
                strainp[2,2] = strainpVar.values[e].data[2]
                strainp[0,1] = strainpVar.values[e].data[3]
                strainp[0,2] = strainpVar.values[e].data[4]
                strainp[1,2] = strainpVar.values[e].data[5]

                strainp[1,0] = strainp[0,1]
                strainp[2,0] = strainp[0,2]
                strainp[2,1] = strainp[1,2]

                # projected plastic shear strain
                gammaP = 2.0*n.dot(strainp.dot(t))

                # projected normal stress
                stressN = n.dot(stress.dot(n))

                storeGammaP[e,sNum] = gammaP
                storeStressN[e,sNum] = stressN
                #print stress[0,0]

if numSteps % 2 == 0:
        numFips = (numSteps - 2)/2
else:
        numFips = (numSteps - 1)/2

deltaGammaP = np.zeros((numelem,numFips))
fip = np.zeros((numelem,numFips))


for f in range(numFips):
        deltaGammaP = abs(storeGammaP[:,2*f+2] - storeGammaP[:,2*f+1] )
        fip[:,f] = abs(0.5*deltaGammaP*(1 + kappa*storeStressN[:,2*f+2]/sy))

print fip
print storeElem

# write data
for e in range(numelem):
        out1.write(str(storeElem[e]))
        for f in range(numFips):
                out1.write(', ' + str(fip[e,f]))
        out1.write('\n')


# close input and output files
out1.close()

odb.close()


