#!/usr/bin/python

import numpy as np
import datetime

def getElemInd(minMaxArray,numNodes,numElements, nodes, elements,nodesPerElem):



    xMin = minMaxArray[0]
    xMax = minMaxArray[1]
    yMin = minMaxArray[2]
    yMax = minMaxArray[3]
    zMin = minMaxArray[4]
    zMax = minMaxArray[5]

    nodeInd = np.empty((numNodes,))
    nodeInd[:] = np.nan

    elemInd = np.empty((numElements,))
    elemInd[:] = np.nan

    #print 'start searching nodes           : ' + str(datetime.datetime.now())

    counter = 0
    for i in range(numNodes):
        xVal = nodes[i,1]
        yVal = nodes[i,2]
        zVal = nodes[i,3]

        if xMin <=  xVal and xVal <= xMax:
            if yMin <=  yVal and yVal <= yMax:
                if zMin <=  zVal and zVal <= zMax:
                    nodeInd[counter] = nodes[i,0]
                    counter = counter + 1

    
    nodeInd = nodeInd[~np.isnan(nodeInd)]

    #print 'end searching nodes             : ' + str(datetime.datetime.now())
    #print 'start sorting nodes             : ' + str(datetime.datetime.now())
    nodeInd.sort()
    #print 'end sorting nodes               : ' + str(datetime.datetime.now())

    #print 'start searching elements        : ' + str(datetime.datetime.now())
    counter = 0
    for e in range(numElements):
        eElement = elements[e,1:nodesPerElem]
        for i in range(nodesPerElem-1):
            if eElement[i] in nodeInd:
                keepElement = True
                break
            else:
                keepElement = False
                #break

        if keepElement:
            elemInd[counter] = elements[e,0]
            counter = counter + 1

    elemInd = elemInd[~np.isnan(elemInd)]
    elemInd = elemInd.astype(np.int64)

    return elemInd
    #print 'end searching elements          : ' + str(datetime.datetime.now())


###############################
######   USER INPUTS  #########
###############################

# input deck name without .inp (i.e., the output of this script)
deckName = 'elsetAllPost'

# element Set name
elsetName = 'nlFip'

#length of edge of square nl volume
dx = 0.333

# Region to creat elset
xGlobalMin = -0.5
xGlobalMax = 0.5
yGlobalMin = -0.5
yGlobalMax = 0.5
zGlobalMin = -0.5
zGlobalMax = 0.5

numNlVolsx = int((xGlobalMax - xGlobalMin)/dx)
numNlVolsy = int((yGlobalMax - yGlobalMin)/dx)
numNlVolsz = int((zGlobalMax - zGlobalMin)/dx)

x = np.linspace(xGlobalMin,xGlobalMax,numNlVolsx+1)
y = np.linspace(yGlobalMin,yGlobalMax,numNlVolsy+1)
z = np.linspace(zGlobalMin,zGlobalMax,numNlVolsz+1)

###############################
######   LOAD FILES   #########
###############################

#abaqus output files
elsetFile = deckName + '-Elsets.inp'
fileOutput = open(elsetFile ,'w')

# node file name (from getElsetNode getElsetNodesElems.py)
nodeFile = 'nlNodes.inp'
# element file name
elementFile = 'nlElements.inp'
# elset file name
elsetFile = 'nlElsets.inp'

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)
matrixElset = np.loadtxt(elsetFile,dtype=int)
matrixElsetInd = matrixElset-1

# assumes element numbers start at 1 with no breaks
elements = elements[matrixElsetInd,:]

numNodes = nodes.shape[0]
print 'Number of Nodes: ' + str(numNodes)

numElements = elements.shape[0]
nodesPerElem = elements.shape[1] - 1
print 'Number of Elements: ' + str(numElements)
print 'Number of Nodes per  Elements: ' + str(nodesPerElem)

total = numNlVolsx*numNlVolsy*numNlVolsz

fipCount = 1
print 'start fip count           : ' + str(datetime.datetime.now())
for ii in range(numNlVolsx):
    for jj in range(numNlVolsy):
        for kk in range(numNlVolsz):
            
            print str(fipCount) + '/' + str(total)
            
            xMin = x[ii]
            xMax = x[ii+1]
            yMin = y[jj]
            yMax = y[jj+1]
            zMin = z[kk]
            zMax = z[kk+1]

            minMaxArray = [xMin, xMax, yMin, yMax, zMin, zMax]

            elemInd = getElemInd(minMaxArray,numNodes,numElements, \
            nodes, elements,nodesPerElem)

            #print 'start writing elements to file  : ' + str(datetime.datetime.now())
            fileOutput.write('\n' + '*Elset, elset=' + elsetName + str(fipCount) + '\n')

            fipCount += 1

            for i in range(len(elemInd)):
                fileOutput.write(str(elemInd[i]) + ', ')
                if i != 0 and i % 15 == 0:
                    fileOutput.write('\n')        
            #print 'end writing elements to file    : ' + str(datetime.datetime.now())                         
            if len(elemInd) < 20:
                print elemInd

print 'end fip count           : ' + str(datetime.datetime.now())

fileOutput.close()

