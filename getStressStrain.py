import math
import datetime
from odbAccess import *
from abaqusConstants import *

# Input and ouput file names
InputName='poroElem1kRndTest'
#InputName='caliElem1kTi64-Z'
#OutName='LE'
#OutName='PE'
OutName1='LE'
OutName2='S'
OutName3='SDV28'
OutName4='SDV30'
#OutName4='SDV72'
OutName5='S'
#OutName6='SDV73'

#tensor component Tij = [T11, T22, T33, T12, T13, T23 ]
#tensor component Ti  = [T(1), T(2), T(3), T(4), T(5), T(6) ]
tensComp = 1
print 'ODB = ' + InputName
print 'Output Variable = ' + OutName1
print 'Output Variable = ' + OutName2
print 'Output Variable = ' + OutName3
print 'Output Variable = ' + OutName4
print 'Output Variable = ' + OutName5
#print 'Output Variable = ' + OutName6

outputfilename1=InputName+'-'+str(OutName1)+'-'+ str(tensComp)+'.txt'
outputfilename2=InputName+'-'+str(OutName2)+'-'+ str(tensComp)+'.txt'
outputfilename3=InputName+'-'+str(OutName3)+'.txt'
outputfilename4=InputName+'-'+str(OutName4)+'.txt'
outputfilename5=InputName+'-'+str(OutName5)+'-Mises' +'.txt'
#outputfilename6=InputName+'-'+str(OutName6)+'.txt'

## open txt file to write to
out1 = open(outputfilename1,'w')
out2 = open(outputfilename2,'w')
out3 = open(outputfilename3,'w')
out4 = open(outputfilename4,'w')
out5 = open(outputfilename5,'w')
#out6 = open(outputfilename6,'w')

# opend .odb file to read from
odb = openOdb(InputName+'.odb')
# number of frames in step
numberFrame = len(odb.steps['Step-1'].frames)

# extract frame 1 to determine how many elements it has
frame = odb.steps['Step-1'].frames[1]
outvar1=frame.fieldOutputs[str(OutName1)]

numelem = len(outvar1.values)

# loop over all times (ie frames)
for ns in range (1,numberFrame):
	# these just let you know its running and how long it takes
	print ns
	print str(datetime.datetime.now())

	# extract varibles desired
	frame = odb.steps['Step-1'].frames[ns]
	outvar1=frame.fieldOutputs[str(OutName1)]
        outvar2=frame.fieldOutputs[str(OutName2)]
	outvar3=frame.fieldOutputs[str(OutName3)]
        outvar4=frame.fieldOutputs[str(OutName4)]
        outvar5=frame.fieldOutputs[str(OutName5)]
        #outvar6=frame.fieldOutputs[str(OutName6)]

        #print outevol.values[1].data
	# average over all elements 
	sumvar1=0
	sumvar2=0
	sumvar3=0
	sumvar4=0
        sumvar5=0
        #sumvar6=0

	for e in range (0,numelem):
		# sum of varible for all elements
		var1 =outvar1.values[e].data[tensComp-1]
		sumvar1 += var1
		var2 = outvar2.values[e].data[tensComp-1]
		sumvar2 += var2
                var3 = outvar3.values[e].data
		sumvar3 += var3
		var4=outvar4.values[e].data
		sumvar4 += var4
                var5=outvar5.values[e].mises
		sumvar5 += var5
                #var6=outvar6.values[e].data
		#sumvar6 += var6

	# average of varibale for all elements	
	avevar1 = sumvar1/numelem
        avevar2 = sumvar2/numelem
	avevar3 = sumvar3/numelem
        avevar4 = sumvar4/numelem
        avevar5 = sumvar5/numelem
        #avevar6 = sumvar6/numelem

	# write data
	out1.write(str(avevar1)+'\n')
        out2.write(str(avevar2)+'\n')
	out3.write(str(avevar3)+'\n')
        out4.write(str(avevar4)+'\n')
        out5.write(str(avevar5)+'\n')
        #out6.write(str(avevar6)+'\n')

# close input and output files
out1.close()
out2.close()
out3.close()
out4.close()
out5.close()
#out6.close()
odb.close()


