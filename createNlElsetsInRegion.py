#!/usr/bin/python

import numpy as np
import datetime

###############################
######   USER INPUTS  #########
###############################

# input deck name without .inp (i.e., the output of this script)
deckName = 'elsetPost'

# element Set name
elsetName = 'nlFip'

# Region to creat elset
xMin = 0.1
xMax = 0.2
yMin = 0.1
yMax = 0.2
zMin = 0.1
zMax = 0.2

# True if nodes or elements are limited to a range
# givine my startNode/Element endNode/Element
nodeRange = True
elementRange = True

startNode = 1
endNode = 103135

startElement =  1301
endElement = 97184

###############################
######   LOAD FILES   #########
###############################

#abaqus output files
elsetFile = deckName + '-Elsets.inp'
fileOutput = open(elsetFile ,'w')

# node file name (from getElsetNode getElsetNodesElems.py)
nodeFile = 'nlNodes.inp'
# element file name
elementFile = 'nlElements.inp'

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)

# assumes node numbers start at 1 with no breaks
if nodeRange == True:
    nodes = nodes[startNode-1 : endNode,:]
if elementRange == True:
    elements = elements[startElement-1 : endElement,:]

#elements = np.array([[1, 1,2,3,4, 5, 6,7,8],[2, 5,6,7,8,9,10,11,12]])
#print elements[0,0]

# nodes = np.array([[1.0, 0.0, 0.0, 0.0], \
#                   [2.0, 0.0, 0.1, 0.0], \
#                   [3.0, 0.0, 0.1, 0.1], \
#                   [4.0, 0.0, 0.0, 0.1], \
#                   [5.0, 0.1, 0.0, 0.0], \
#                   [6.0, 0.1, 0.1, 0.0], \
#                   [7.0, 0.1, 0.1, 0.1], \
#                   [8.0, 0.1, 0.0, 0.1], \
#                   [9.0, 0.2, 0.0, 0.0], \
#                   [10.0, 0.2, 0.1, 0.0], \
#                   [11.0, 0.2, 0.1, 0.1],\
#                   [12.0, 0.2, 0.0, 0.1]])

numNodes = nodes.shape[0]
print 'Number of Nodes: ' + str(numNodes)

numElements = elements.shape[0]
nodesPerElem = elements.shape[1] - 1
print 'Number of Elements: ' + str(numElements)
print 'Number of Nodes per  Elements: ' + str(nodesPerElem)

nodeInd = np.empty((numNodes,))
nodeInd[:] = np.nan

elemInd = np.empty((numElements,))
elemInd[:] = np.nan

print 'start searching nodes           : ' + str(datetime.datetime.now())

counter = 0
for i in range(numNodes):
    xVal = nodes[i,1]
    yVal = nodes[i,2]
    zVal = nodes[i,3]
    if xMin <=  xVal and xVal <= xMax:
        if yMin <=  yVal and yVal <= yMax:
            if zMin <=  zVal and zVal <= zMax:
                nodeInd[counter] = nodes[i,0]
                counter = counter + 1
nodeInd = nodeInd[~np.isnan(nodeInd)]

print 'end searching nodes             : ' + str(datetime.datetime.now())
print 'start sorting nodes             : ' + str(datetime.datetime.now())
nodeInd.sort()
print 'end sorting nodes               : ' + str(datetime.datetime.now())

print 'start searching elements        : ' + str(datetime.datetime.now())
counter = 0
for e in range(numElements):
    eElement = elements[e,1:nodesPerElem]
    for i in range(nodesPerElem-1):
        if eElement[i] in nodeInd:
            keepElement = True
        else:
            keepElement = False
            break

    if keepElement:
        elemInd[counter] = elements[e,0]
        counter = counter + 1

elemInd = elemInd[~np.isnan(elemInd)]
elemInd = elemInd.astype(np.int64)

print 'end searching elements          : ' + str(datetime.datetime.now())

print 'start writing elements to file  : ' + str(datetime.datetime.now())
fileOutput.write('*Elset, elset=' + elsetName + '\n')

for i in range(len(elemInd)):
    fileOutput.write(str(elemInd[i]) + ', ')
    if i != 0 and i % 15 == 0:
        fileOutput.write('\n')        
print 'end writing elements to file    : ' + str(datetime.datetime.now())                         
if len(elemInd) < 20:
    print elemInd

fileOutput.close()

