import math
import datetime
from odbAccess import *
from abaqusConstants import *

#elset
#elsetName = 'PM_MASK_3'
#elsetName = 'AVEELSETTHIRD3'
#elsetName = 'AVEELSETSIXTH2'
elsetName = 'AVEELSETTENTH10'
# Input and ouput file names
InputName='polymer1-900k-run'
#OutName='LE'
#OutName='PE'
OutName='S'
#tensor component Tij = [T11, T22, T33, T12, T13, T23 ]
#tensor component Ti  = [T(1), T(2), T(3), T(4), T(5), T(6) ]
tensComp = 2
print elsetName
print 'ODB = ' + InputName
print 'Output Variable = ' + OutName
#outputfilename=InputName+'-'+str(OutName)+'-'+ str(tensComp)+'.txt'
outputfilename=InputName+'-'+str(OutName)+'-Mises.txt'

# open txt file to write to
out = open(outputfilename,'w')
# opend .dob file to read from
odb = openOdb(InputName+'.odb')

# assmebly
assembly = odb.rootAssembly.instances['PART-1-1']
print assembly.elementSets
elemset = assembly.elementSets[elsetName]

# number of frames in step
numberFrame = len(odb.steps['Step-1'].frames)

# extract frame 1 to determine how many elements it has
frame = odb.steps['Step-1'].frames[1]
outvar=frame.fieldOutputs[str(OutName)]
#further extract from elset
outvar = outvar.getSubset(region=elemset)

numelem = len(outvar.values)

# loop over all times (ie frames)
for ns in range (1,numberFrame):
	# these just let you know its running and how long it takes
	print ns
	print str(datetime.datetime.now())

	# extract varibles desired
	frame = odb.steps['Step-1'].frames[ns]
	outvar=frame.fieldOutputs[str(OutName)]
        outvar=frame.fieldOutputs[str(OutName)]
        #further extract from elset
        outvar = outvar.getSubset(region=elemset)
        
        #print outevol.values[1].data
	# average over all elements 
	sumvar=0
	
	for e in range (0,numelem):
		# sum of varible for all elements
		var=outvar.values[e].mises
            
		sumvar += var

	# average of varibale for all elements	
	avevar = sumvar/numelem
	# write data
	out.write(str(avevar)+'\n')

# close input and output files
out.close()
odb.close()


