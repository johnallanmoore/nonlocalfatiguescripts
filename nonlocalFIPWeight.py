import math
import datetime

import numpy as np
import sys

try:
    sampleNum = sys.argv[1] #  var1
    elemNum = sys.argv[2] #  var2
except:
    pass

print(sampleNum)

# length scale
L = 0.13365046175

# Input and ouput file names
InputName='paraFipMesh'  

#print 'Output Variable = ' + OutName4

outputfilename1=InputName+'-nonlocalFIP_NCW'+ str(sampleNum) + '_' + str(elemNum) +'.txt'

## open txt file to write to
out1 = open(outputfilename1,'w')

elsetsFile = 'paraFipMesh_' + str(elemNum) +'_NC-ElsetsMat.inp'

elsetsMat = np.loadtxt(elsetsFile,delimiter=',',dtype=int)

elsetsCentRFile = 'paraFipMesh_' + str(elemNum) +'-ElsetsCentR.inp'
elsetsCentR = np.loadtxt(elsetsCentRFile,delimiter=',')


numVols = len(elsetsMat[:,0])

fipFile = 'paraFipMesh-FIP_'+ str(sampleNum) + '_' + str(elemNum)  + '.txt'
localFipMat = np.loadtxt(fipFile,delimiter=',')

numSteps = len(localFipMat[0,:])-1
numElem  = len(localFipMat[:,0])


nonlocalFipMat = np.zeros((numVols,numSteps))

for s in range(numSteps):
        print('start step ' + str(s+1))
        for f in range(numVols):
                elset = elsetsMat[f,:]
                elset = elset[np.nonzero(elset)]
                centR = elsetsCentR[f,:]
                centR = centR[~np.isnan(centR)]
                # assumes all element in volume are roughly the same size
                numElemVol = len(elset)

                sumVal = 0
                Ax = 0
                counter = 0
                for e in elset:
                        elemInd = np.where(localFipMat[:,0] == e)
                        phi = np.exp(-centR[counter]**2.0/L**2.0)
                        counter = counter + 1
                        for gp in elemInd:
                                sumVal += localFipMat[gp,s+1]*phi
                                Ax += phi
                aveVal = sumVal/Ax
                nonlocalFipMat[f,s] = aveVal

# write data

for f in range(numVols):
        for s in range(numSteps):
                if s == numSteps-1:
                        out1.write(str(nonlocalFipMat[f,s]))
                else:
                        out1.write(str(nonlocalFipMat[f,s]) + ', ')
        out1.write('\n')

print("max fip in last step" + str(max(nonlocalFipMat[:,-1])))


# close input and output files
out1.close()



