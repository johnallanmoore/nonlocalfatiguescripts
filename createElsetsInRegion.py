#!/usr/bin/python

import numpy as np
import datetime

###############################
######   USER INPUTS  #########
###############################
quad = 'Tenth10'

# input deck name without .inp
deckName = 'elsetPost' + quad

# element Set name
elsetName = 'aveElset' + quad

# Region to creat elset
xMin = -5000.
xMax =  5000.
yMin = -5000.
yMax =  5000.
if quad == 'Third1':
    zMin =  -5000.
    zMax =  843.3
if quad == 'Third2':
    zMin =  843.3
    zMax =  1686.7
if quad == 'Third3':
    zMin =  1686.7
    zMax =  5000.0
if quad == 'Sixth2':
    zMin =  421.7
    zMax =  843.3 
if quad == 'Tenth1':
    zMin =  -5000.
    zMax =  253. 
if quad == 'Tenth2':
    zMin =  253.
    zMax =  506.
if quad == 'Tenth3':
    zMin =  506.
    zMax =  759.
if quad == 'Tenth4':
    zMin =  759.
    zMax =  1012.
if quad == 'Tenth5':
    zMin =  1012.
    zMax =  1265.
if quad == 'Tenth6':
    zMin =  1265.
    zMax =  1518.
if quad == 'Tenth7':
    zMin =  1518.
    zMax =  1771.
if quad == 'Tenth8':
    zMin =  1771.
    zMax =  2024.
if quad == 'Tenth9':
    zMin =  2024.
    zMax =  2277.
if quad == 'Tenth10':
    zMin =  2277.
    zMax =  5000.

print quad
# True if nodes or elements are limited to a range
# givine my startNode/Element endNode/Element
nodeRange = False
elementRange = True

startNode = 1
endNode = 12

startElement =  494354
endElement = 642659

###############################
######   LOAD FILES   #########
###############################

#abaqus output files
elsetFile = deckName + '-Elsets.inp'
fileOutput = open(elsetFile ,'w')

# node file name (from getElsetNode getElsetNodesElems.py)
nodeFile = 'elsetNodes.inp'
# element file name
elementFile = 'elsetElements.inp'

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)

# assumes node numbers start at 1 with no breaks
if nodeRange == True:
    nodes = nodes[startNode-1 : endNode,:]
if elementRange == True:
    elements = elements[startElement-1 : endElement,:]

#elements = np.array([[1, 1,2,3,4, 5, 6,7,8],[2, 5,6,7,8,9,10,11,12]])
#print elements[0,0]

# nodes = np.array([[1.0, 0.0, 0.0, 0.0], \
#                   [2.0, 0.0, 0.1, 0.0], \
#                   [3.0, 0.0, 0.1, 0.1], \
#                   [4.0, 0.0, 0.0, 0.1], \
#                   [5.0, 0.1, 0.0, 0.0], \
#                   [6.0, 0.1, 0.1, 0.0], \
#                   [7.0, 0.1, 0.1, 0.1], \
#                   [8.0, 0.1, 0.0, 0.1], \
#                   [9.0, 0.2, 0.0, 0.0], \
#                   [10.0, 0.2, 0.1, 0.0], \
#                   [11.0, 0.2, 0.1, 0.1],\
#                   [12.0, 0.2, 0.0, 0.1]])

numNodes = nodes.shape[0]
print 'Number of Nodes: ' + str(numNodes)

numElements = elements.shape[0]
nodesPerElem = elements.shape[1] - 1
print 'Number of Elements: ' + str(numElements)
print 'Number of Nodes per  Elements: ' + str(nodesPerElem)

nodeInd = np.empty((numNodes,))
nodeInd[:] = np.nan

elemInd = np.empty((numElements,))
elemInd[:] = np.nan

print 'start searching nodes           : ' + str(datetime.datetime.now())

counter = 0
for i in range(numNodes):
    xVal = nodes[i,1]
    yVal = nodes[i,2]
    zVal = nodes[i,3]
    if xMin <=  xVal and xVal <= xMax:
        if yMin <=  yVal and yVal <= yMax:
            if zMin <=  zVal and zVal <= zMax:
                nodeInd[counter] = nodes[i,0]
                counter = counter + 1
nodeInd = nodeInd[~np.isnan(nodeInd)]

print 'end searching nodes             : ' + str(datetime.datetime.now())
print 'start sorting nodes             : ' + str(datetime.datetime.now())
nodeInd.sort()
print 'end sorting nodes               : ' + str(datetime.datetime.now())

print 'start searching elements        : ' + str(datetime.datetime.now())
counter = 0
for e in range(numElements):
    eElement = elements[e,1:nodesPerElem]
    for i in range(nodesPerElem-1):
        if eElement[i] in nodeInd:
            keepElement = True
        else:
            keepElement = False
            break

    if keepElement:
        elemInd[counter] = elements[e,0]
        counter = counter + 1

elemInd = elemInd[~np.isnan(elemInd)]
elemInd = elemInd.astype(np.int64)

print 'end searching elements          : ' + str(datetime.datetime.now())

print 'start writing elements to file  : ' + str(datetime.datetime.now())
fileOutput.write('*Elset, elset=' + elsetName + '\n')

for i in range(len(elemInd)):
    fileOutput.write(str(elemInd[i]) + ', ')
    if i != 0 and i % 15 == 0:
        fileOutput.write('\n')        
print 'end writing elements to file    : ' + str(datetime.datetime.now())                         
if len(elemInd) < 20:
    print elemInd

fileOutput.close()

