import math
import datetime
import time

import numpy as np
import sys

try:
    sampleNum = sys.argv[1] #  var1
    elemNum = sys.argv[2] #  var2
except:
    pass

print(sampleNum)

longWay = False

# Input and ouput file names
InputName='paraFipMesh'  

isNonConf = True

#print 'Output Variable = ' + OutName4
if isNonConf == True:
    outputfilename1=InputName+'-nonlocalFIP_NC'+ str(sampleNum) + '_' + str(elemNum) +'.txt'
else:
    outputfilename1=InputName+'-nonlocalFIP_'+ str(sampleNum) + '_' + str(elemNum) +'.txt'

## open txt file to write to
out1 = open(outputfilename1,'w')

if isNonConf == True:
    elsetsFile = 'paraFipMesh_' + str(elemNum) +'_NC-ElsetsMat.inp'
else:
    elsetsFile = 'paraFipMesh_' + str(elemNum) +'-ElsetsMat.inp'
elsetsMat = np.loadtxt(elsetsFile,delimiter=',',dtype=int)

numVols = len(elsetsMat[:,0])

fipFile = 'paraFipMesh-FIP_'+ str(sampleNum) + '_' + str(elemNum)  + '.txt'
localFipMat = np.loadtxt(fipFile,delimiter=',')

numSteps = len(localFipMat[0,:])-1
numElem  = len(localFipMat[:,0])


nonlocalFipMat = np.zeros((numVols,numSteps))

for s in range(numSteps):
        print('start step ' + str(s+1))
        t1 = time.time()

        
        for f in range(numVols):
                telset1 = time.time()
                elset = elsetsMat[f,:]
                elset = elset[np.nonzero(elset)]
                telset2 = time.time()
                #print('time to load elsets ' + str(telset2-telset1))
                
                # assumes all element in volume are roughly the same size
                numElemVol = len(elset)

                sumVal = 0
                tsum1 = time.time()
                for e in elset:
                        if longWay == True:
                            elemInd = np.where(localFipMat[:,0] == e)
                            for gp in elemInd:
                                sumVal += localFipMat[gp,s+1]
                        else:
                            sumVal = sumVal + localFipMat[e-1,s+1]
                tsum2 = time.time()
                #print('time to find element ' + str(tsum2-tsum1))
        
                aveVal = sumVal/numElemVol
                nonlocalFipMat[f,s] = aveVal
        t2 = time.time()
        print(t2-t1)
# write data

for f in range(numVols):
        for s in range(numSteps):
                if s == numSteps-1:
                        out1.write(str(nonlocalFipMat[f,s]))
                else:
                        out1.write(str(nonlocalFipMat[f,s]) + ', ')
        out1.write('\n')

print("max fip in last step" + str(max(nonlocalFipMat[:,-1])))


# close input and output files
out1.close()



